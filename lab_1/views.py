from django.shortcuts import render
from datetime import datetime, date

curr_year = int(datetime.now().strftime("%Y"))

mhs_name = 'Muhammad Al-Kautsar Maktub' 
birth_date = date(1999, 1, 18) 
npm = 1706043885 
tempat_kuliah = 'Universitas Indonesia'
hobi = 'Mempelajari hal-hal yang baru'
deskripsi = 'Saya berkuliah di UI. Saya orang yang suka mempelajari \
hal-hal baru.'

friend_name_1 = 'Sendhy Nugroho' 
friend_birth_date_1 = date(1999, 1, 29) 
friend_npm_1 = 1706013632
friend_hobi_1 = 'Olahraga'
friend_deskripsi_1 = 'Orangnya sans banget, suka banget bola \
terutama Real Madrid, suka main futsal,\
gampang banget bosen, sering banget begadang gak jelas'

friend_name_2 = 'Yudha Ananda Rahayu Putra' 
friend_birth_date_2 = date(1999, 10, 2) 
friend_npm_2 = 1706984796
friend_hobi_2 = 'Olahraga & Bikin Film'
friend_deskripsi_2 = 'Dia YUDHA bukan YUDA.'

def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
    'tempatkuliah' : tempat_kuliah, 'hobi' : hobi, 'deskripsi' : deskripsi,
    'friendname_1' : friend_name_1, 'friendage_1' : calculate_age(friend_birth_date_1.year),
    'friendnpm_1' : friend_npm_1, 'friendhobi_1' : friend_hobi_1,
    'frienddeskripsi_1' : friend_deskripsi_1, 
    'friendname_2' : friend_name_2, 'friendage_2' : calculate_age(friend_birth_date_2.year),
    'friendnpm_2' : friend_npm_2, 'friendhobi_2' : friend_hobi_2,
    'frienddeskripsi_2' : friend_deskripsi_2}

    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
